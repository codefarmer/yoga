package com.sel.yoga.common.wechat.constant;

import java.util.Arrays;
import java.util.Optional;

/**
 * @program: yoga-parent
 * @description: 消息类型枚举 https://mp.weixin.qq.com/wiki?t=resource/res_main&id=mp1421140547
 * @author: frode.zhao
 * @create: 2019-06-02 18:10
 **/
public enum  MessageTypeEnum {

    MSG_TYPE_TEXT("text","文本消息"),
    MSG_TYPE_IMAGE("image","图片消息"),
    MSG_TYPE_VOICE("voice","语音消息"),
    MSG_TYPE_VIDEO("video","视频消息"),
    MSG_TYPE_MUSIC("music","音乐消息"),
    MSG_TYPE_NEWS("news","图文消息（点击跳转到图文消息页面）"),
    MSG_TYPE_MPNEWS("mpnews","图文消息（点击跳转到外链）"),
    MSG_TYPE_WXCARD("wxcard","卡券消息"),
    MSG_TYPE_MINIPROGRAMPAGE("miniprogrampage","小程序"),
    MSG_TYPE_OTHER("other","未知");

    //找不到码值时，为其他
    public static String getDesc(String code){

        Optional<MessageTypeEnum> first = Arrays.stream(MessageTypeEnum.values())
                .filter(value -> value.getCode().equals(code))
                .findFirst();
        return first.orElse(MSG_TYPE_OTHER).getDesc();
    }

    private String code;
    private String desc;

    MessageTypeEnum(String code, String desc) {
        this.code = code;
        this.desc = desc;
    }
    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }
}
