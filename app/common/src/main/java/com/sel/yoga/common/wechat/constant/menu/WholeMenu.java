package com.sel.yoga.common.wechat.constant.menu;

/**
 * @program: yoga-parent
 * @description: 整个菜单对象的封装
 * @author: frode.zhao
 * @create: 2019-06-02 19:22
 **/
public class WholeMenu {

    private BaseMenu[] button;

    public BaseMenu[] getButton() {
        return button;
    }

    public void setButton(BaseMenu[] button) {
        this.button = button;
    }

}
