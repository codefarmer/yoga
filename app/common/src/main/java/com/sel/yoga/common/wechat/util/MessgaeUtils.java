package com.sel.yoga.common.wechat.util;

import org.dom4j.Document;
import org.dom4j.DocumentException;
import org.dom4j.Element;
import org.dom4j.io.SAXReader;

import javax.servlet.http.HttpServletRequest;
import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @program: yoga-parent
 * @description: 将请求转化为Map对象
 * @author: frode.zhao
 * @create: 2019-06-02 17:54
 **/
public class MessgaeUtils {

    /*将xml格式转化为map*/
    public static Map<String,String> xmlToMap(HttpServletRequest request){
        Map<String,String> result = new HashMap<>();
        try {
            SAXReader reader = new SAXReader();
            InputStream inputStream = request.getInputStream();
            Document doc = reader.read(inputStream);
            Element root = doc.getRootElement();//得到根节点
            List<Element> list = root.elements();//根节点下的所有的节点
            for(Element e:list){
                result.put(e.getName(),e.getText());
            }
            inputStream.close();
        }catch (DocumentException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return result;
    }
}