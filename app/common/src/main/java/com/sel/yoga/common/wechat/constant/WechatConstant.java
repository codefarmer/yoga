package com.sel.yoga.common.wechat.constant;

/**
 * @program: yoga-parent
 * @description: 微信请求内置规定参数
 * @author: frode.zhao
 * @create: 2019-06-02 18:30
 **/
public class WechatConstant {

    public static final String SIGNATURE = "signature";
    public static final String TIMESTAMP = "timestamp";
    public static final String NONCE = "nonce";
    public static final String ECHOSTR = "echostr";
    public static final String MSGID = "MsgId";
    public static final String MSGTYPE = "MsgType";
    public static final String CONTENT = "Content";
    public static final String CREATETIME = "CreateTime";
    public static final String TOUSERNAME = "ToUserName";
    public static final String FROMUSERNAME = "FromUserName";



    public static final String ACCESS_TOKEN = "access_token";
    public static final String EXPIRES_IN = "expires_in";


    public static final String ERRCODE = "errcode";
    public static final String ERRMSG = "errmsg";

}
