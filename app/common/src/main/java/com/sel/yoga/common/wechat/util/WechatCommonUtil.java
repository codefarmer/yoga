package com.sel.yoga.common.wechat.util;

import com.alibaba.fastjson.JSONException;
import com.alibaba.fastjson.JSONObject;
import com.sel.yoga.common.util.HttpUtil;
import com.sel.yoga.common.wechat.constant.WechatConstant;
import com.sel.yoga.common.wechat.constant.WechatUrlConstant;
import com.sel.yoga.common.wechat.constant.menu.WholeMenu;
import com.sel.yoga.common.wechat.model.AccessToken;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


/**
 * @program: yoga-parent
 * @description: 通用工具类
 * @author: frode.zhao
 * @create: 2019-06-02 13:13
 **/
public class WechatCommonUtil {

    private static Logger logger = LoggerFactory.getLogger(WechatCommonUtil.class);

    /**
     * 创建菜单
     * @param menu 菜单实例
     * @param accessToken 有效的access_token
     * @return 0表示成功，其他值表示失败
     */
    public static int createMenu(WholeMenu menu, String accessToken) {
        int result = 0;
        // 拼装创建菜单的url
        String url = WechatUrlConstant.USER_DEFINE_MENU_CREATE_URL + accessToken;
        // 将菜单对象转换成json字符串
        String jsonMenu = JSONObject.toJSONString(menu).toString();
        // 调用接口创建菜单
        JSONObject jsonObject = HttpUtil.doRequest(url, HttpUtil.REQUEST_METHOD_POST, jsonMenu);
        if (null != jsonObject) {
            if (0 != jsonObject.getIntValue(WechatConstant.ERRCODE)) {
                result = jsonObject.getIntValue(WechatConstant.ERRCODE);
                logger.error("创建菜单失败 errcode:{} errmsg:{}", jsonObject.getIntValue(WechatConstant.ERRCODE), jsonObject.getString(WechatConstant.ERRMSG));
            }
        }
        return result;
    }

    /**
     * 获取接口访问凭证
     * @param appid 凭证
     * @param appsecret 密钥
     * @return
     */
    public static AccessToken getAccessToken(String appid, String appsecret) {
        AccessToken accessToken = null;
        String requestUrl = WechatUrlConstant.GET_ACCESS_TOKEN_URL.replace("APPID", appid).replace("APPSECRET", appsecret);
        // 发起GET请求获取凭证
        JSONObject jsonObject = HttpUtil.doRequest(requestUrl, HttpUtil.REQUEST_METHOD_GET, null);
        if (null != jsonObject) {
            try {
                accessToken = new AccessToken();
                accessToken.setAccessToken(jsonObject.getString(WechatConstant.ACCESS_TOKEN));
                accessToken.setExpiresIn(jsonObject.getIntValue(WechatConstant.EXPIRES_IN));
            } catch (JSONException e) {
                accessToken = null;
                // 获取token失败
                logger.error("获取token失败 errcode:{} errmsg:{}", jsonObject.getIntValue(WechatConstant.ERRCODE), jsonObject.getString(WechatConstant.ERRMSG));
            }
        }
        return accessToken;
    }
}