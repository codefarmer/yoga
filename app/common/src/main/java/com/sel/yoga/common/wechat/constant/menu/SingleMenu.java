package com.sel.yoga.common.wechat.constant.menu;

/**
 * @program: yoga-parent
 * @description: 单一菜单：没有子菜单的菜单项
 * @author: frode.zhao
 * @create: 2019-06-02 19:16
 **/
public class SingleMenu extends BaseMenu{

    private String type;
    private String key;

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }
}
