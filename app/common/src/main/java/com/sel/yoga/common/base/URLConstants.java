package com.sel.yoga.common.base;

public class URLConstants {

    /**
     * rest接口地址前缀
     */
    public static final String REST_API_PREFIX = "/yoga/webapi";
    /**
     * response encoding
     */
    public static final String DEFAULT_CHARSET = "UTF-8";
    /**
     * Content-Type
     */
    public static final String DEFAULT_CONTENT_TYPE = "application/json;charset=" + DEFAULT_CHARSET;
}
