package com.sel.yoga.common.wechat.constant.menu;

/**
 * @program: yoga-parent
 * @description: 菜单项的基类
 * @author: frode.zhao
 * @create: 2019-06-02 19:15
 **/
public class BaseMenu {

    private String name;//所有一级菜单、二级菜单都共有一个相同的属性，那就是name

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

}
