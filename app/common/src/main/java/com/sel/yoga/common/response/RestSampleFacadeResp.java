package com.sel.yoga.common.response;

public class RestSampleFacadeResp<T> extends AbstractFacadeResp {

    /**
     * response的返回对象数据
     */
    private T data;

    public RestSampleFacadeResp() {
        super(false);
    }

    public RestSampleFacadeResp(boolean success) {
        super(success);
    }

    public RestSampleFacadeResp(T data, boolean success) {
        super(success);
        this.data = data;
    }

    public RestSampleFacadeResp(T data, boolean success, Exception exception) {
        this(data, success);
        this.setException(exception);
    }

    public T getData() {
        return data;
    }

    public void setData(T data) {
        this.data = data;
    }

    public static RestSampleFacadeResp<Object> createSystemExceptionResp(){
        RestSampleFacadeResp<Object> resp = new RestSampleFacadeResp<>(false);
        resp.setResultMsg("系统异常");
        return resp;
    }

    public static RestSampleFacadeResp<Object> createSystemExceptionResp(Exception exception){
        RestSampleFacadeResp<Object> resp = createSystemExceptionResp();
        resp.setException(exception);
        return resp;
    }

}
