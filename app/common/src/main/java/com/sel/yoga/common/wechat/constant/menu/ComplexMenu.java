package com.sel.yoga.common.wechat.constant.menu;

/**
 * @program: yoga-parent
 * @description: 复合菜单
 * @author: frode.zhao
 * @create: 2019-06-02 19:18
 **/
public class ComplexMenu extends BaseMenu{

    private BaseMenu[] sub_button;

    public BaseMenu[] getSub_button() {
        return sub_button;
    }

    public void setSub_button(BaseMenu[] sub_button) {
        this.sub_button = sub_button;
    }

}
