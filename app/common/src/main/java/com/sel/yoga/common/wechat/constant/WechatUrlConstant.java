package com.sel.yoga.common.wechat.constant;

/**
 * @program: yoga-parent
 * @description: 请求微信url地址
 * @author: frode.zhao
 * @create: 2019-06-02 19:09
 **/
public class WechatUrlConstant {

    /** 获取access_token的接口地址（GET） 限200（次/天）*/
    public final static String GET_ACCESS_TOKEN_URL = "https://api.weixin.qq.com/cgi-bin/token?grant_type=client_credential&appid=APPID&secret=APPSECRET";


    /** 自定义菜单创建接口 POST https协议 限100（次/天）*/
    public final static String USER_DEFINE_MENU_CREATE_URL = "https://api.weixin.qq.com/cgi-bin/menu/create?access_token=";
    /** 自定义菜单查询接口 GET  http协议 */
    public final static String USER_DEFINE_MENU_GET_URL = "https://api.weixin.qq.com/cgi-bin/menu/get?access_token=";
    /** 自定义菜单删除接口 GET http协议 */
    public final static String USER_DEFINE_MENU_DELETE_URL = "https://api.weixin.qq.com/cgi-bin/menu/delete?access_token=";

}
