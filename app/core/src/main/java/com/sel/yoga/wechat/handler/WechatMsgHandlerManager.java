package com.sel.yoga.wechat.handler;

import com.alibaba.fastjson.JSONObject;
import com.sel.yoga.common.wechat.constant.WechatConstant;
import com.sel.yoga.common.wechat.util.MessgaeUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import javax.servlet.http.HttpServletRequest;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;

/**
 * @program: yoga-parent
 * @description: 微信处理器管理类
 * @author: frode.zhao
 * @create: 2019-06-02 18:53
 **/
//@Component
public class WechatMsgHandlerManager {

    private static final Logger logger = LoggerFactory.getLogger(WechatMsgHandlerManager.class);

    @Autowired
    private Set<WechatMsgHandler> wechatMsgHandlers;

    public static ConcurrentMap<String,WechatMsgHandler > wechatMsgHandlerMap = new ConcurrentHashMap<>();

    @PostConstruct
    public void init(){
        if(wechatMsgHandlers != null) {
            for (WechatMsgHandler wechatMsgHandler : wechatMsgHandlers) {
                wechatMsgHandler.setMsgType();
                wechatMsgHandlerMap.put(wechatMsgHandler.getMsgType(),wechatMsgHandler);
            }
        }
    }

    public boolean handleReceiveMsg(HttpServletRequest request){
        Map<String, String> messageMap = MessgaeUtils.xmlToMap(request);
        WechatMsgHandler wechatMsgHandler = wechatMsgHandlerMap.get(messageMap.get(WechatConstant.MSGTYPE));
        if(wechatMsgHandler == null ){
            System.out.println("24141");
            logger.error("根据msgType没有找到对应的消息处理器"+ JSONObject.toJSONString(messageMap));
            return false;
        }
        wechatMsgHandler.handleReceiveMsg(messageMap.get(WechatConstant.CONTENT));
        return true;
    }
}
