package com.sel.yoga.wechat.service;

import com.sel.yoga.common.wechat.constant.menu.BaseMenu;
import com.sel.yoga.common.wechat.constant.menu.ComplexMenu;
import com.sel.yoga.common.wechat.constant.menu.SingleMenu;
import com.sel.yoga.common.wechat.constant.menu.WholeMenu;
import com.sel.yoga.common.wechat.model.AccessToken;
import com.sel.yoga.common.wechat.util.WechatCommonUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

/**
 * @description: 菜单
 * @author: frode.zhao
 * @create: 2019-06-02 19:47
 **/
@Service
public class MenuService {
    
    private static Logger log = LoggerFactory.getLogger(MenuService.class);

    public static void main(String[] args) {
        // 第三方用户唯一凭证
        String appId = "000000000000000000";
        // 第三方用户唯一凭证密钥
        String appSecret = "00000000000000000000000000000000";

        // 调用接口获取access_token
        AccessToken at = WechatCommonUtil.getAccessToken(appId, appSecret);

        if (null != at) {
            // 调用接口创建菜单
            int result = WechatCommonUtil.createMenu(getMenu(), at.getAccessToken());

            // 判断菜单创建结果
            if (0 == result)
                log.info("菜单创建成功！");
            else
                log.info("菜单创建失败，错误码：" + result);
        }
    }

    /**
     * 组装菜单数据
     *
     * @return
     */
    public static WholeMenu getMenu() {
        SingleMenu btn1 = new SingleMenu();
        btn1.setName("天气预报");
        btn1.setType("click");
        btn1.setKey("10");

        SingleMenu btn21 = new SingleMenu();
        btn21.setName("聊天唠嗑");
        btn21.setType("click");
        btn21.setKey("21");

        SingleMenu btn22 = new SingleMenu();
        btn22.setName("幽默笑话");
        btn22.setType("click");
        btn22.setKey("22");

        SingleMenu btn3 = new SingleMenu();
        btn3.setName("电影排行榜");
        btn3.setType("click");
        btn3.setKey("3");




        /**
         * 微信：  mainBtn1,mainBtn2,mainBtn3底部的三个一级菜单。
         */
        ComplexMenu mainBtn1 = new ComplexMenu();
        mainBtn1.setName("生活助手");
        //一级下有4个子菜单
        mainBtn1.setSub_button(new SingleMenu[] { btn1});

        ComplexMenu mainBtn2 = new ComplexMenu();
        mainBtn2.setName("休闲驿站");
        mainBtn2.setSub_button(new SingleMenu[] { btn21, btn22});

        ComplexMenu mainBtn3 = new ComplexMenu();
        mainBtn3.setName("更多体验");
        mainBtn3.setSub_button(new SingleMenu[] { btn3});


        /**
         * 封装整个菜单
         */
        WholeMenu menu = new WholeMenu();
        menu.setButton(new BaseMenu[] { mainBtn1, mainBtn2, mainBtn3 });
        return menu;
    }
}