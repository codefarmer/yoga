package com.sel.yoga.wechat.handler;

/**
 * @program: yoga-parent
 * @description:
 * @author: frode.zhao
 * @create: 2019-06-02 18:48
 **/
public abstract class WechatMsgHandler implements AbstractWechatMsgHandler {

    protected String msgType;

    @Override
    public void handleReceiveMsg(String msgContent){

    }

    public String getMsgType() {
        return msgType;
    }
    public abstract void setMsgType();

}
