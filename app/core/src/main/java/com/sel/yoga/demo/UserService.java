package com.sel.yoga.demo;

import java.util.List;

public interface UserService {

    List<UserDO> getAll();

}
