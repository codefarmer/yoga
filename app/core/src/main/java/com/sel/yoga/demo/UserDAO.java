package com.sel.yoga.demo;

import java.util.List;

public interface UserDAO {

    List<UserDO> getAll();
}
