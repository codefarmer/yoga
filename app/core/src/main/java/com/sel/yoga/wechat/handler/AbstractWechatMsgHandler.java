package com.sel.yoga.wechat.handler;

/**
 * @program: yoga-parent
 * @description: 微信消息处理器
 * @author: frode.zhao
 * @create: 2019-06-02 18:42
 **/
public interface AbstractWechatMsgHandler {

    void handleReceiveMsg(String msgContent);

}
