package com.sel.yoga.endpoint.api.wechat;

import com.alibaba.fastjson.JSONObject;
import com.sel.yoga.common.base.URLConstants;
import com.sel.yoga.common.wechat.constant.WechatConstant;
import com.sel.yoga.common.wechat.model.AccessToken;
import com.sel.yoga.common.wechat.util.CheckUitl;
import com.sel.yoga.common.wechat.util.MessgaeUtils;
import com.sel.yoga.common.wechat.util.WechatCommonUtil;
import com.sel.yoga.wechat.handler.WechatMsgHandlerManager;
import com.sel.yoga.wechat.service.MenuService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Map;

@RequestMapping(URLConstants.REST_API_PREFIX + "/wechat")
@RestController
public class WeChatController {

//    @Autowired
//    private WechatMsgHandlerManager wechatMsgHandlerManager;
    @Autowired
    private MenuService menuService;
    @Value("${wechatAppID}")
    private String wechatAppID;
    @Value("${wechatAppSecret}")
    private String wechatAppSecret;

    /**
     * 接入开发走这个GET
     */
    @RequestMapping(value = "/validate", method = RequestMethod.GET)
    public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        //接收微信用来校验信息的内置规定参数
        Map<String, String> messageMap = MessgaeUtils.xmlToMap(request);
        //按微信指定的规则进行校验并做出响应
        if(CheckUitl.checkSignature(messageMap.get(WechatConstant.SIGNATURE), WechatConstant.TIMESTAMP, WechatConstant.NONCE)){
            response.getWriter().print(messageMap.get(WechatConstant.ECHOSTR));
        }
    }

    /**
     * 配置RequestMethod.POST，用于接收处理消息
     */
    @RequestMapping(value = "/validate", method = RequestMethod.POST,produces = "application/xml;charset=UTF-8")
    public void doPost(HttpServletRequest request,HttpServletResponse response) throws IOException{
        //还没有回复
//        wechatMsgHandlerManager.handleReceiveMsg(request);
    }

    /**
     * 配置微信菜单
     */
    @RequestMapping(value = "/setMenu", method = RequestMethod.GET)
    public void setMenu(HttpServletRequest request,HttpServletResponse response) throws IOException{
        AccessToken accessToken = WechatCommonUtil.getAccessToken(wechatAppID, wechatAppSecret);
        System.out.println(JSONObject.toJSONString(accessToken));
        if (null != accessToken) {
            // 调用接口创建菜单
            int result = WechatCommonUtil.createMenu(MenuService.getMenu(), "22_gG7DwbrZgUlNdGjzbeq_S16hautW_wv0sPBBLR_E3MKQdDeiwry36ah0ejDuko145dNicclJ50jubmWICBjNi6-bB-IXdUH8PQD_G-dhNEwPMXGX1DERp4_-kIiBB9Ka3olkXPcg80x-cNK-TQVhAAAHEJ");
            // 判断菜单创建结果
            if (0 == result){
                System.out.println("菜单创建成功！");
            } else{
                System.out.println("菜单创建失败，错误码："+result);
            }
        }
    }

    /**
     * 配置微信菜单
     */
    @RequestMapping(value = "/test", method = RequestMethod.GET)
    public void test(HttpServletRequest request,HttpServletResponse response) throws IOException{
        System.out.println("=========test");
    }


}
