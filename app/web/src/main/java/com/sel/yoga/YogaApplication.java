package com.sel.yoga;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ImportResource;

@SpringBootApplication
@ImportResource({"classpath*:META-INF/yoga/*.xml"})
@MapperScan("com.sel.*")
public class YogaApplication {

	public static void main(String[] args) {
	    try {
            System.out.println("【启动开始】==========");
            SpringApplication.run(YogaApplication.class, args);
            System.out.println("【启动完成】==========");
        } catch (Throwable t){
            System.out.println("【启动报错】==========");
            System.out.println(t);
        }
	}

}
